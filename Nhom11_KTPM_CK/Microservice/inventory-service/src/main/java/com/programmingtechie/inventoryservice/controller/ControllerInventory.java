package com.programmingtechie.inventoryservice.controller;

import com.programmingtechie.inventoryservice.service.ServiceInventory;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/inventory")
@RequiredArgsConstructor
public class ControllerInventory {
    private final ServiceInventory serviceInventory;

    @GetMapping("/{sku-code}")
    public boolean inStock(@PathVariable("sku-code") String skuCode){
        return serviceInventory.inStock(skuCode);
    }
}
