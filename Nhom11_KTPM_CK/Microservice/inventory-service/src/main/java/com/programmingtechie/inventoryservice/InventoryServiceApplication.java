package com.programmingtechie.inventoryservice;

import com.programmingtechie.inventoryservice.model.Inventory;
import com.programmingtechie.inventoryservice.repository.RepositoryInventory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class InventoryServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(InventoryServiceApplication.class, args);
    }

//    @Bean
//    public CommandLineRunner loadRunner(RepositoryInventory repositoryInventory){
//        return args -> {
//        Inventory inventory = new Inventory();
//        inventory.setSkuCode("iphone_11");
//        inventory.setQuantity(3);
//
//
//        Inventory inventory1 = new Inventory();
//        inventory1.setSkuCode("iphone_15Pro");
//        inventory1.setQuantity(1);
//
//
//        repositoryInventory.save(inventory);
//        repositoryInventory.save(inventory1);
//        };
//    }

}
