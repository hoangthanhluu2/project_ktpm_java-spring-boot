package com.programmingtechie.inventoryservice.repository;

import com.programmingtechie.inventoryservice.model.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RepositoryInventory extends JpaRepository<Inventory,Long> {


//    Optional<Inventory> findBySkuCodeIn(List<String> skuCode);sử dụng cái này chỉ để kiểm tra true hoặ false sử dụng bên dưới để lấy ra 1 ds dữ liệu
    List<Inventory> findBySkuCodeIn(List<String> skuCode);//đối tượng cần lấy lên là DS nên k dùng Optional
}
