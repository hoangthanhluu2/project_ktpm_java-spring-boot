package com.programmingtechie.productservice.controller;

import com.programmingtechie.productservice.Product.Product;
import com.programmingtechie.productservice.dto.ProductReponse;
import com.programmingtechie.productservice.dto.Productquest;
import com.programmingtechie.productservice.service.ServiceProduct;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
public class ControllerService {
    private final ServiceProduct serviceProduct;
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Productquest productquest){
        serviceProduct.createProduct(productquest);
    }

    @GetMapping
    public List<ProductReponse> getProduct(){
        return serviceProduct.getProduct();
    }

}
