package com.programmingtechie.productservice.repository;

import com.programmingtechie.productservice.Product.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product,String> {
}
