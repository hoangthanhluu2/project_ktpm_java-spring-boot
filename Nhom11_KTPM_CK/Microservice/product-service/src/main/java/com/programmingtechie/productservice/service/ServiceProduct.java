package com.programmingtechie.productservice.service;

import com.programmingtechie.productservice.Product.Product;
import com.programmingtechie.productservice.dto.ProductReponse;
import com.programmingtechie.productservice.dto.Productquest;
import com.programmingtechie.productservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ServiceProduct {
    private final ProductRepository productRepository;

    public void createProduct(Productquest productquest) {
        Product product = Product.builder()
                .name(productquest.getName())
                .description(productquest.getDescription())
                .price(productquest.getPrice())

                .build();
        productRepository.save(product);
    }

    public List<ProductReponse> getProduct() {
        List<Product> productList = productRepository.findAll();

      return productList.stream().map(product -> mapGetProduct(product)).toList();
    }

    private ProductReponse mapGetProduct(Product product) {//trả ra là ProductReponse nhưng dữ liệu nạp vao contructor là một List product
        return ProductReponse.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .build();

    }


}
