package com.programmingtechie.orderservice.service;

import com.programmingtechie.orderservice.dto.OrderLineItemsReponseDto;
import com.programmingtechie.orderservice.dto.OrderLineItemsRequestDto;
import com.programmingtechie.orderservice.dto.OrderReponse;
import com.programmingtechie.orderservice.dto.OrderRequest;
import com.programmingtechie.orderservice.model.Order;
import com.programmingtechie.orderservice.model.OrderLineItems;
import com.programmingtechie.orderservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    public void placeOrder(OrderRequest orderRequest) {
        Order order = new Order();
        order.setOrderNumber(UUID.randomUUID().toString());

        List<OrderLineItems> orderListItems = orderRequest.getOrderLineItemsRequestDtos().stream()
                .map(orderListRequestDto -> mapListOrder(orderListRequestDto)).toList();

    order.setOrderLineItems(orderListItems);
     orderRepository.save(order);
    }

    private OrderLineItems mapListOrder(OrderLineItemsRequestDto orderListRequestDto) {
            OrderLineItems orderLineItems = new OrderLineItems();
            orderLineItems.setId(orderListRequestDto.getId());
        orderLineItems.setQuantity(orderListRequestDto.getQuantity());
        orderLineItems.setPrice(orderListRequestDto.getPrice());
        orderLineItems.setSkuCode(orderListRequestDto.getSkuCode());
            return  orderLineItems;
    }

    public List<OrderReponse> getOrder() {
        List<Order> orders = orderRepository.findAll();

        List<OrderReponse> orderReponses = orders.stream().map(orderList -> mapGetList(orderList)).toList();

return  orderReponses;
    }

    private OrderReponse mapGetList(Order orderList) {
        OrderReponse orderReponse = new OrderReponse();
        orderReponse.setOrderNumber(orderList.getOrderNumber());
        orderReponse.setId(orderList.getId());

        List<OrderLineItemsReponseDto> list = orderList.getOrderLineItems().stream().map(orderListLineItems -> mapGetListLineItems(orderListLineItems)).toList();
        orderReponse.setOrderLineItemsReponseDtos(list);
        return orderReponse;

    }

    private OrderLineItemsReponseDto mapGetListLineItems(OrderLineItems orderListLineItems) {
        OrderLineItemsReponseDto orderLineItemsReponseDto = new OrderLineItemsReponseDto();
        orderLineItemsReponseDto.setId(orderListLineItems.getId());
        orderLineItemsReponseDto.setPrice(orderListLineItems.getPrice());
        orderLineItemsReponseDto.setQuantity(orderListLineItems.getQuantity());
        orderLineItemsReponseDto.setSkuCode(orderListLineItems.getSkuCode());

        return orderLineItemsReponseDto;

    }
}
