package com.programmingtechie.productservice.dto;

import lombok.*;

import java.math.BigDecimal;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductReponse {
    private String id;
    private String name;
    private String description;
    private BigDecimal price;
}
