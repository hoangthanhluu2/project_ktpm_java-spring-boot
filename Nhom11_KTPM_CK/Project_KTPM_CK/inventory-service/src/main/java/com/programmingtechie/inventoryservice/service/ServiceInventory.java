package com.programmingtechie.inventoryservice.service;

import com.programmingtechie.inventoryservice.dto.InventoryReponse;
import com.programmingtechie.inventoryservice.repository.RepositoryInventory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ServiceInventory {
    private final RepositoryInventory repositoryInventory;
    @Transactional(readOnly = true)
    public List<InventoryReponse> inStock(List<String> skuCode) {
        log.info("Checking Inventory");
        return repositoryInventory.findBySkuCodeIn(skuCode)
                .stream().map(inventory ->
                   InventoryReponse.builder()
                            .skuCode(inventory.getSkuCode())
                            .isStock(inventory.getQuantity() > 0)//kiem tra quantity(so luong)
                            .build()
                ).toList();




    }
}
