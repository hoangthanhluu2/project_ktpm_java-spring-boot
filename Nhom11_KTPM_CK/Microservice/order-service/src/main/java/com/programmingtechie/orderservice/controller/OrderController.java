package com.programmingtechie.orderservice.controller;

import com.programmingtechie.orderservice.dto.OrderReponse;
import com.programmingtechie.orderservice.dto.OrderRequest;
import com.programmingtechie.orderservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/order")
@RestController
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @PostMapping
    public String placeOrder(@RequestBody OrderRequest orderRequest){
        orderService.placeOrder(orderRequest);
        return "Bạn đã đặt hàng thành công";
    }

    @GetMapping
    public List<OrderReponse> getOrder(){
        return orderService.getOrder();
    }
}
