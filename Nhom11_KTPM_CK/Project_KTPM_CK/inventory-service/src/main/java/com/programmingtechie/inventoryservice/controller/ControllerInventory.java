package com.programmingtechie.inventoryservice.controller;

import com.programmingtechie.inventoryservice.dto.InventoryReponse;
import com.programmingtechie.inventoryservice.service.ServiceInventory;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/inventory")
@RequiredArgsConstructor
public class ControllerInventory {
    private final ServiceInventory serviceInventory;

//    @GetMapping("/{sku-code}") kiểm tra xem mã sản phẩm đưa về có còn trong kho và trả ra
    @GetMapping//chú thích ở trên
    public List<InventoryReponse> inStock(@RequestParam List<String> skuCode){
        return serviceInventory.inStock(skuCode);
    }
}
