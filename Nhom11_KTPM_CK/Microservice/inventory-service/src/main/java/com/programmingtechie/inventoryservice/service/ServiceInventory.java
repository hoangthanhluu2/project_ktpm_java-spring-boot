package com.programmingtechie.inventoryservice.service;

import com.programmingtechie.inventoryservice.repository.RepositoryInventory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service

@RequiredArgsConstructor
public class ServiceInventory {
    private final RepositoryInventory repositoryInventory;
    @Transactional(readOnly = true)
    public boolean inStock(String skuCode) {
        return repositoryInventory.findBySkuCode(skuCode).isPresent();
    }
}
